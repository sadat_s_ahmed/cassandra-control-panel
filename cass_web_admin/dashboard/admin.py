from django.contrib import admin
from .models import MBean, Cluster, Datacenter, Rack, Node

# Register your models here.
admin.site.register(MBean)
admin.site.register(Cluster)
admin.site.register(Datacenter)
admin.site.register(Rack)
admin.site.register(Node)
