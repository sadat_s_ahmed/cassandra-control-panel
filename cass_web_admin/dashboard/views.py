from django.shortcuts import render
from django.template import loader
from django.http import HttpResponse, Http404

import requests
import json

from .models import MBean, Cluster, Node

# Create your views here.
def index(request):
  clusters = Cluster.objects.all()
  return render(request, 'index.html', {"clusters":clusters})

def clusterDetail(request, cluster):
  clusterData = Cluster.objects.get(pk=cluster)
  return render(request, "cluster.html", {"cluster":clusterData})

def nodeDetail(request, node):
  nodeData = Node.objects.get(pk=node)
  return render(request, 'node.html', {"node": nodeData})

def pollGraphData(request, cluster):
  nodes = Node.objects.filter(parent=cluster)
  jmxConns = []
  for node in nodes:
    jmxConns.append(node.node_jmx_url+"/")
  beans = MBean.objects.all()
  beansData = []
  for bean in beans:
    if bean.status == True:
      beansData.append(bean.getJson())

  responses = []
  for jmxConn in jmxConns:
    req = requests.post(jmxConn, data=json.dumps(beansData))
    responses.append(json.loads(req.content))

  results = []
  for resp in responses:
    count = 0
    for i in range(len(beans)):
      if beans[i].status == True:
	results.append(resp[count])
	count = count + 1

  return HttpResponse(json.dumps(results), content_type='application/json')

def execCommand(request):
  return HttpResponse(status=200)
