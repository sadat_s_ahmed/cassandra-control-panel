from __future__ import unicode_literals

from django.db import models
import json

class NetworkBlock(models.Model):
  block_name = models.CharField(max_length=128, unique=True)
  block_type = models.IntegerField(default=0)
  
  BLOCK_CLUSTER = 1
  BLOCK_REGION = 2
  BLOCK_DATACENTER = 3
  BLOCK_RACK = 4
  BLOCK_NODE = 5

  @classmethod
  def create(cls, blockName, blockType):
    block = cls(block_name=blockName, block_type=blockType)
    return block

  def __str__(self):
    if block_type == BLOCK_CLUSTER:
      return "cluster: "+self.block_name
    else if block_type == BLOCK_REGION:
      return "region: "+self.block_name
    else if block_type == BLOCK_DATACENTER:
      return "datacenter: "+self.block_name
    else if block_type == BLOCK_RACK:
      return "rack: "+self.block_name
    else if block_type == BLOCK_NODE:
      return "node: "+self.block_name
    else
      return "unknown: "+self.block_name

class Cluster(models.Model):
  network_block = models.ForeignKey(NetworkBlock, on_delete=models.CASCADE)

  @classmethod
  def create(cls, clusterName):
    block = NetworkBlock.create(clusterName, NetworkBlock.BLOCK_CLUSTER)
    cluster = cls(network_block=block)
    return cluster

  def __str__(self):
    return self.network_block.__str__()

class Region(models.Model):
  network_block = models.ForeignKey(NetworkBlock, on_delete=models.CASCADE)
  parent = models.ForeignKey(NetworkBlock, on_delete=models.CASCADE)

  @classmethod
  def create(cls, parentBlock, regionName):
    block = NetworkBlock.create(regionName, NetworkBlock.BLOCK_REGION)
    region = cls(network_block=block, parent=parentBlock)
    return region

  def __str__(self):
    return self.network_block.__str__()+" parent:"+self.parent.__str__()


class Datacenter(models.Model):
  network_block = models.ForeignKey(NetworkBlock, on_delete=models.CASCADE)
  parent = models.ForeignKey(NetworkBlock, on_delete=models.CASCADE)

  @classmethod
  def create(cls, parentBlock, datacenterName):
    block = NetworkBlock.create(datacenterName, NetworkBlock.BLOCK_DATACENTER)
    datacenter = cls(network_block=block, parent=parentBlock)
    return datacenter

  def __str__():
    return self.network_block.__str__()+" parent:"+self.parent.__str__()

class Rack(models.Model):
  network_block = models.ForeignKey(NetworkBlock, on_delete=models.CASCADE)
  parent = models.ForeignKey(NetworkBlock, on_delete=models.CASCADE)

  @classmethod
  def create(cls, parentBlock, rackName):
    block = NetworkBlock.create(rackName, NetworkBlock.BLOCK_RACK)
    rack = cls(network_block=block, parent=parentBlock)
    return rack

  def __str__():
    return self.network_block.__str__()+" parent:"+self.parent.__str__()

class Node(models.Model):
  network_block = models.ForeignKey(NetworkBlock, on_delete=models.CASCADE)
  parent = models.ForeignKey(NetworkBlock, on_delete=models.CASCADE)

  @classmethod
  def create(cls, parentBlock, nodeName):
    block = NetworkBlock.create(rackName, NetworkBlock.BLOCK_NODE)
    node = cls(network_block=block, parent=parentBlock)
    return rack

  def __str__():
    return self.network_block.__str__()+" parent:"+self.parent.__str__()
