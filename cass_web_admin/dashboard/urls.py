from django.conf.urls import url

from . import views

app_name = 'dashboard'
urlpatterns = [
  url(r'^$', views.index, name='index'),
  url(r'^cluster/(?P<cluster>[0-9]*)$', views.clusterDetail, name='cluster'),
  url(r'^node/(?P<node>[0-9]*)$', views.nodeDetail, name='node'),
  url(r'^poll/(?P<cluster>[0-9]*)$', views.pollGraphData, name='pollGraph'),
]
