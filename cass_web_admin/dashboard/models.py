from __future__ import unicode_literals

from django.db import models
import json

# Create your models here.
class MBean(models.Model):
  mbean = models.CharField(max_length=512)
  attribute = models.CharField(max_length=128)
  path = models.CharField(max_length=128, blank=True)
  type = models.CharField(max_length=16)
  status = models.BooleanField(default=True)

  @classmethod
  def create(cls, mbean, attribute, path, type):
    mbean = cls(mbean=mbean,attribute=attribute,path=path,type=type)
    return mbean

  def __str__(self):
    return json.dumps(self.getJson())
    
  def getJson(self):
    data = {"type":self.type, 
      "mbean":self.mbean, 
      "attribute":self.attribute, 
      "path":self.path}
    return data

class NetworkBlock(models.Model):

  BLOCK_TYPES = (
   (1, 'Cluster'),
   (2, 'Datacenter'),
   (3, 'Rack'),
   (4, 'Node')
  )
  block_name = models.CharField(max_length=128)
  block_type = models.IntegerField(default=0, choices=BLOCK_TYPES)
  
  BLOCK_CLUSTER = 1
  BLOCK_DATACENTER = 2
  BLOCK_RACK = 3
  BLOCK_NODE = 4

  def __str__(self):
    index = self.block_type -1
    resultTuple = NetworkBlock.BLOCK_TYPES[index]
    result = resultTuple[1]
    return "%s(%s)" % (self.block_name,result)

class Cluster(NetworkBlock):
  cluster_type = models.TextField(max_length=32)
  cluster_version = models.TextField(max_length=16)

  def getDatacenters(self):
    datacenters = Datacenter.objects.filter(parent=self)
    return datacenters

class Datacenter(NetworkBlock):
  parent = models.ForeignKey(Cluster, on_delete=models.CASCADE, related_name="%(class)s_")

  def getRacks(self):
    racks = Rack.objects.filter(parent=self)
    return racks

class Rack(NetworkBlock):
  parent = models.ForeignKey(Datacenter, on_delete=models.CASCADE, related_name="%(class)s_")

  def getNodes(self):
    nodes = Node.objects.filter(parent=self)
    return nodes

  def getRacks(self):
    racks = Rack.objects.filter(parent=self)
    return racks

class Node(NetworkBlock):
  parent = models.ForeignKey(Rack, on_delete=models.CASCADE, related_name="%(class)s_")
  mbeans = models.ManyToManyField(MBean)


